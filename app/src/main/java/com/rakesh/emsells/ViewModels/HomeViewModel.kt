package com.rakesh.emsells.ViewModels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rakesh.emsells.Helper.RetrofitBuilderClass
import com.rakesh.emsells.Helper.RetrofitInterface
import com.rakesh.emsells.MainActivity.Companion.mKeyHomeListArray
import com.rakesh.emsells.MainActivity.Companion.mTitleHomeListArray
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel: ViewModel() {


    private var mHomeListData=MutableLiveData<JSONObject>()
    fun getHomeListDataFromServer():MutableLiveData<JSONObject>{

        mHomeListData= MutableLiveData()

        loadHomeListDataFromServer()

        return mHomeListData
    }

    private fun loadHomeListDataFromServer() {

        val Input=HashMap<String,String>()
        Input.put("Action","5")
        Input.put("FormCode","Cus12")

        RetrofitBuilderClass().getRetrofitClient().create(RetrofitInterface::class.java)
            .getHomeListData("",Input).enqueue(object : Callback<ResponseBody>{
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful){

                        val fullobject=JSONObject(response.body()!!.string())

                        val TableArray=fullobject.getJSONArray("Table")

                            val eachObject=TableArray.getJSONObject(0)
                            val kjhbhnjm = eachObject.toString().replace("{","").replace("}","").split(",").toList()
                            val keyObject = ArrayList<String>()
                            val dataObject = ArrayList<String>()
                            kjhbhnjm.forEach {
                                val keyList = it.split(":")
                                for (check in 0 until keyList.size) {

                                    if (check % 2 == 0) {
                                        keyObject.add(keyList.get(check))
                                    } else {
                                        dataObject.add(keyList.get(check))

                                    }

                                }
                            }

                        Log.d("xdfcgvhbjnkm", keyObject.toString())
                        Log.d("dfgvhbjnm", dataObject.toString())

                        mKeyHomeListArray=keyObject
                        mTitleHomeListArray=dataObject


                        mKeyHomeListArray.add("")
                        mKeyHomeListArray.add("")

                        mTitleHomeListArray.add("")
                        mTitleHomeListArray.add("")

                        mHomeListData.value=fullobject



                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

            })
    }
}