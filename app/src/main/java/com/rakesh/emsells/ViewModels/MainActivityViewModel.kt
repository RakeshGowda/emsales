package com.rakesh.emsells.ViewModels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rakesh.emsells.Helper.RetrofitBuilderClass
import com.rakesh.emsells.Helper.RetrofitInterface
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityViewModel: ViewModel() {

    private var mSideMenuJsonArray = MutableLiveData<JSONArray>()
    fun getSideMenuListFromServer(): MutableLiveData<JSONArray>{

        mSideMenuJsonArray= MutableLiveData()

        loadSideMenu()

        return mSideMenuJsonArray
    }

    private fun loadSideMenu() {

        val _InputParams=HashMap<String,String>()
        _InputParams.put("RoleId","7")
        _InputParams.put("LanguageID","1")
        _InputParams.put("TenantId","1")
        Log.d("_InputParams",_InputParams.toString())


        RetrofitBuilderClass().getRetrofitClient().create(RetrofitInterface::class.java)
            .sideMenuList("",_InputParams).enqueue(object : Callback<ResponseBody>{
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {

                    if (response.isSuccessful){

                        mSideMenuJsonArray.value=JSONArray(response.body()!!.string())

                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

            })
    }
}