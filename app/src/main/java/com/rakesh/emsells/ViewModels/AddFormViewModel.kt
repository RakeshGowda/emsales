package com.rakesh.emsells.ViewModels

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.rakesh.emsells.Helper.RetrofitBuilderClass
import com.rakesh.emsells.Helper.RetrofitInterface
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddFormViewModel(application: Application) : AndroidViewModel(application) {

    private val mContext = application.applicationContext
    private var mAddFormViews: MutableLiveData<JSONArray> = MutableLiveData()
    private var mAddFormDropDown: MutableLiveData<JSONArray> = MutableLiveData()


    fun getAddViewDetailsFromServer(): MutableLiveData<JSONArray> {
        mAddFormViews = MutableLiveData()
        loadAddViewDetails()

        return mAddFormViews
    }

    fun getAddFormDropDownDetailsFromServer(
        formCode: String,
        fieldCode: String
    ): MutableLiveData<JSONArray> {
        mAddFormDropDown= MutableLiveData()
        loadDropDownData(formCode, fieldCode)

        return mAddFormDropDown
    }

    private fun loadAddViewDetails() {

        val Input = HashMap<String, String>()
        //Input.put("FormCode", "Cus12")
        Input.put("FormCode", "Cli20")


        RetrofitBuilderClass().getRetrofitClient().create(RetrofitInterface::class.java)
            .getAddViewDetails("", Input).enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {

                    if (response.isSuccessful) {

                        val jsonArray = JSONArray(response.body()!!.string().toString())
                        val jsonDataObject = jsonArray.getJSONObject(0)
                        if (jsonDataObject.has("JsonData")) {
                            val strJsonData = jsonDataObject.getString("JsonData")
                            val JsonData = JSONObject(strJsonData)
                            if (JsonData.has("FormCode") && JsonData.has("Createjson")) {
                                val FormCode = JsonData.getString("FormCode")


                                val mCreatejson = JsonData.getJSONArray("Createjson")



                                mAddFormViews.value = mCreatejson

                            }
                        } else {

                        }

                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

            })
    }

    private fun loadDropDownData(formCode: String, fieldCode: String) {

        val Input = HashMap<String, String>()
        //Input.put("FormCode", "Cus12")
        Input.put("FormCode", formCode)
        Input.put("FieldCode", fieldCode)
        Input.put("Value", "")
        Input.put("Status", "")


        RetrofitBuilderClass().getRetrofitClient().create(RetrofitInterface::class.java)
            .getAddFormDropDownDetails("", Input).enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {

                    if (response.isSuccessful) {

                        val jsArray = JSONArray(response.body()!!.string())
                        mAddFormDropDown.value = jsArray
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

            })
    }


}