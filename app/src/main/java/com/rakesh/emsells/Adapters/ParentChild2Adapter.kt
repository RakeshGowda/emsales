package com.rakesh.emsells.Adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.rakesh.emsells.Models.LeftNavDataClass
import com.rakesh.emsells.R
import kotlinx.android.synthetic.main.layout_child2.view.*


class ParentChild2Adapter(
    context: Context,
    listLeftNavChild1: ArrayList<LeftNavDataClass>,
    ParentPosition: Int
) :
    RecyclerView.Adapter<ParentChild2Adapter.ViewHolder>() {
    private val mContext = context
    private val mListLeftNavChild1 = listLeftNavChild1
    private val mParentPosition = ParentPosition


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = LayoutInflater.from(mContext).inflate(R.layout.layout_child2, parent, false)
        return ViewHolder(mView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tv_childlastmodulename.text=mListLeftNavChild1.get(mParentPosition).ListLeftNavChild2.get(
            position
        ).ModuleName
        if (mListLeftNavChild1.get(mParentPosition).ListLeftNavChild2.get(position).IsParent){

            val params: LinearLayout.LayoutParams =
                LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(16, 16, 0, 0)
            holder.itemView.tv_childlastmodulename.typeface= Typeface.DEFAULT_BOLD
            holder.itemView.tv_childlastmodulename.setLayoutParams(params)

        }
    }

    override fun getItemCount(): Int {
        return mListLeftNavChild1.get(mParentPosition).ListLeftNavChild2.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}