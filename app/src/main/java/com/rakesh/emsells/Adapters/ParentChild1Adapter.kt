package com.rakesh.emsells.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rakesh.emsells.Models.LeftNavDataClass
import com.rakesh.emsells.R
import kotlinx.android.synthetic.main.layout_parentchild1.view.*
import kotlinx.android.synthetic.main.parent_expand.view.*

class ParentChild1Adapter(
    context: Context,
    listLeftNavChild1: ArrayList<LeftNavDataClass>,
    position: Int
): RecyclerView.Adapter<ParentChild1Adapter.ViewHolder>() {

   private val mContext=context
    private val mListLeftNavChild1=listLeftNavChild1
    private val mParentPosition=position
    private var isexpended=false


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mChild1View=LayoutInflater.from(mContext).inflate(R.layout.layout_parentchild1,parent,false)
        return ViewHolder(mChild1View)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tv_childmodulename.text=mListLeftNavChild1.get(mParentPosition).ListLeftNavChild1.get(position).ModuleName
        val mParentChild2Adapter=ParentChild2Adapter(mContext,mListLeftNavChild1,mParentPosition)
        holder.itemView.rv_parentchild2.adapter=mParentChild2Adapter


        if(isexpended){
            holder.itemView.rv_parentchild2.visibility=View.VISIBLE
        }else{
            holder.itemView.rv_parentchild2.visibility=View.GONE
        }

        holder.itemView.setOnClickListener {

            if (isexpended){
                isexpended=false
            }else{
                isexpended=true
            }
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return mListLeftNavChild1.get(mParentPosition).ListLeftNavChild1.size
    }


    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

    }
}