package com.rakesh.emsells.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rakesh.emsells.Models.LeftNavDataClass
import com.rakesh.emsells.R
import kotlinx.android.synthetic.main.parent_expand.view.*


class ParentAdapter(val context: Context, mArrayLeftNavDataClass: ArrayList<LeftNavDataClass>): RecyclerView.Adapter<ParentAdapter.ViewHolder>() {
    private val mContext=context
    private val mArrayLeftNav=mArrayLeftNavDataClass
    private var isexpended=false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView=LayoutInflater.from(mContext).inflate(R.layout.parent_expand,parent,false)
        return ViewHolder(mView)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tv_parentmodulename.text=mArrayLeftNav.get(position).ModuleName

        val mParentChild1Adapter=ParentChild1Adapter(mContext,mArrayLeftNav,position)
        holder.itemView.rv_parentchild1.adapter=mParentChild1Adapter


        if(isexpended){
            holder.itemView.rv_parentchild1.visibility=View.VISIBLE
        }else{
            holder.itemView.rv_parentchild1.visibility=View.GONE
        }

        holder.itemView.setOnClickListener {

            if (isexpended){
                isexpended=false
            }else{
                isexpended=true
            }
            notifyItemChanged(position)
        }


    }

    override fun getItemCount(): Int {

        return mArrayLeftNav.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}
