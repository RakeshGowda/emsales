package com.rakesh.emsells.Adapters

import android.content.Context
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rakesh.emsells.MainActivity.Companion.mKeyHomeListArray
import com.rakesh.emsells.MainActivity.Companion.mTitleHomeListArray
import com.rakesh.emsells.R
import kotlinx.android.synthetic.main.homelistachildadpater.view.*
import org.json.JSONObject

class HomeListAdapter(context: Context, jsonObjectServer: JSONObject): RecyclerView.Adapter<HomeListAdapter.ViewHolder>() {

    private val mContext=context
    private val mHomeListServer=jsonObjectServer

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView=LayoutInflater.from(mContext).inflate(R.layout.homelistachildadpater,parent,false)
        return ViewHolder(mView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.ll_dynamicviewattach.removeAllViews()

        val contentLayout = LinearLayout(mContext)
        contentLayout.apply {
            contentLayout.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER
            setPadding(8, 8, 8, 8)
        }

        contentLayout.removeAllViews()

       // val dataObject=mHomeListServer.getJSONArray("data")

        val rowView=LayoutInflater.from(mContext).inflate(R.layout.listchildhorizontal,null,false)
        val rowtitle=rowView.findViewById<TextView>(R.id.tv_titlelist)
        val rowdata=rowView.findViewById<TextView>(R.id.tv_datalist)


        mKeyHomeListArray.forEachIndexed { index, s ->

            for (indData in 0 until mHomeListServer.length() ){
               // val indObject=dataObject.getJSONObject(indData)
               // val indObject=dataObject.getJSONObject(indData)
               // if (mHomeListServer.toString().get(s)){
                    rowtitle.text=mTitleHomeListArray.get(index)

                Log.d("row_data", mTitleHomeListArray.get(index))
                    //rowdata.text=mHomeListServer.getString(s)
                contentLayout.removeAllViews()
                    contentLayout.addView(rowView)
               // }
            }
        }

        holder.itemView.ll_dynamicviewattach.addView(contentLayout)


    }

    override fun getItemCount(): Int {
        return 5
    }

    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

    }
}