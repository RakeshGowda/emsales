package com.rakesh.emsells.Models

data class LeftNavDataClass (
    val ModuleID:String,
    val ModuleName:String,
   // val ListLeftNavParent:ArrayList<LeftNavChild>,
    val ListLeftNavChild1:ArrayList<LeftNavChild>,
    val ListLeftNavChild2:ArrayList<LeftNavChild1>

)

data class LeftNavChild(
    val ModuleID:String,
    val ModuleName:String,

    )

data class LeftNavChild1(
    val ModuleID:String,
    val ModuleName:String,
    val IsParent:Boolean
    )