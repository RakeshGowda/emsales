package com.rakesh.emsells

import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rakesh.emsells.Adapters.ParentAdapter
import com.rakesh.emsells.Models.LeftNavChild
import com.rakesh.emsells.Models.LeftNavChild1
import com.rakesh.emsells.Models.LeftNavDataClass
import com.rakesh.emsells.ViewModels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray

class MainActivity : AppCompatActivity() {

    companion object {
        lateinit var mMainActivity: MainActivity
        lateinit var mKeyHomeListArray: ArrayList<String>
        lateinit var mTitleHomeListArray: ArrayList<String>

        lateinit var navController: NavController

    }

    private lateinit var mLeftNavDataClass: LeftNavDataClass
    private lateinit var mArrayLeftNavDataClass: ArrayList<LeftNavDataClass>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        mMainActivity = this

        navController=findNavController(R.id.nav_host_fragment)


        getSideMenuListFromServer()
    }

    private fun getSideMenuListFromServer() {

        val mViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        mViewModel.getSideMenuListFromServer().observe(this, {
            if (it != null) {

                setUpDataClassForLeftNavSetup(it!!)

            }
        })

    }

    private fun setUpDataClassForLeftNavSetup(responseArray: JSONArray) {

        mArrayLeftNavDataClass = ArrayList()

        val mListLeftNavParent = ArrayList<LeftNavChild>()
        for (i in 0 until responseArray.length()) {
            val induvitualobject = responseArray.getJSONObject(i)

            if (induvitualobject.has("ModuleId") && induvitualobject.has("ModuleName")) {
                val paretntModuleId = induvitualobject.getString("ModuleId")
                val paretntModuleName = induvitualobject.getString("ModuleName")

                mListLeftNavParent.add(LeftNavChild(paretntModuleId, paretntModuleName))
                val LeftNavChild = ArrayList<LeftNavChild>()
                if (induvitualobject.has("children")) {
                    val parentchildrenarray = induvitualobject.getJSONArray("children")


                    for (children in 0 until parentchildrenarray.length()) {
                        val induvitualchildrenobject = parentchildrenarray.getJSONObject(children)

                        var LeftNavChild1 = ArrayList<LeftNavChild1>()
                        if (induvitualchildrenobject.has("ModuleId") && induvitualchildrenobject.has(
                                "ModuleName"
                            )
                        ) {

                            val childModuleId = induvitualchildrenobject.getString("ModuleId")
                            val childModuleName = induvitualchildrenobject.getString("ModuleName")

                            LeftNavChild.add(LeftNavChild(childModuleId, childModuleName))
                            LeftNavChild1 = ArrayList()
                            if (induvitualchildrenobject.has("children1")) {
                                val children1array =
                                    induvitualchildrenobject.getJSONArray("children1")

                                for (child1 in 0 until children1array.length()) {
                                    val children1object = children1array.getJSONObject(child1)

                                    if (children1object.has("ModuleId") && children1object.has("ModuleName")) {

                                        val child1ModuleId = children1object.getString("ModuleId")
                                        val child1ModuleName =
                                            children1object.getString("ModuleName")
                                        val child1ParentFormCode =
                                            children1object.getString("ParentFormCode")

                                        if (child1ModuleId.equals(child1ParentFormCode)) {
                                            LeftNavChild1.add(
                                                LeftNavChild1(
                                                    child1ModuleId,
                                                    child1ModuleName,
                                                    true
                                                )
                                            )
                                        } else {
                                            LeftNavChild1.add(
                                                LeftNavChild1(
                                                    child1ModuleId,
                                                    child1ModuleName,
                                                    false
                                                )
                                            )
                                        }
                                    }
                                }

                                mLeftNavDataClass = LeftNavDataClass(
                                    paretntModuleId,
                                    paretntModuleName,
                                    LeftNavChild,
                                    LeftNavChild1
                                )
                                mArrayLeftNavDataClass.add(mLeftNavDataClass)

                            }
                        }
                    }

                }

            }

        }


        Log.d("responseforleftnav", mArrayLeftNavDataClass.toString())

        setupRecyclerView()


    }

    private fun setupRecyclerView() {

        val parentAdapter = ParentAdapter(this, mArrayLeftNavDataClass)
        el_left_nav.adapter = parentAdapter

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}