package com.rakesh.emsells.Screens

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.rakesh.emsells.Helper.DynamicViewHelper
import com.rakesh.emsells.MainActivity.Companion.mMainActivity
import com.rakesh.emsells.Models.arraySubFormDataClas
import com.rakesh.emsells.R
import com.rakesh.emsells.ViewModels.AddFormViewModel
import org.json.JSONArray
import org.json.JSONObject
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class AddFormFragment : Fragment() {

    private val mDynamicViewHelper = DynamicViewHelper()
    private var allViewInstance: ArrayList<View> = ArrayList()

    private var viewTypes: ArrayList<String> = ArrayList()

    private var aListofKeyObject = ArrayList<String>()

    private var aListofMandateObjectCheck = ArrayList<String>()

    private lateinit var globalSubFormListView: LinearLayout

    private var indexSubFormPosition = 0

    private lateinit var llRootaddform: LinearLayout

    private var listSubFormDetails = ArrayList<arraySubFormDataClas>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_second, container, false)

        llRootaddform = mView.findViewById(R.id.ll_addform)

        val addViewModel = ViewModelProvider(this).get(AddFormViewModel::class.java)
        addViewModel.getAddViewDetailsFromServer().observe(viewLifecycleOwner, {
            if (it != null) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    createAddFormDynamicView(it, llRootaddform)
                }


            }
        })


        return mView
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun createAddFormDynamicView(it: JSONArray, llAddform: LinearLayout) {

        listSubFormDetails.clear()

        globalSubFormListView = LinearLayout(mMainActivity)
        globalSubFormListView.apply {
            globalSubFormListView.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER
            setPadding(8, 8, 8, 8)
        }


        val contentLayout = LinearLayout(mMainActivity)
        contentLayout.apply {
            contentLayout.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER
            setPadding(8, 8, 8, 8)
        }


        val contentSubFormLayout = LinearLayout(mMainActivity)
        contentSubFormLayout.apply {
            val mSubFormLayoutlayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mSubFormLayoutlayoutParams.topMargin = 32
            mSubFormLayoutlayoutParams.marginStart = 32
            mSubFormLayoutlayoutParams.marginEnd = 32

            layoutParams = mSubFormLayoutlayoutParams

            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER
            setPadding(0, 16, 0, 16)
        }

        for (crateJsonArrayIndex in 0 until it.length()) {
            val createJsonObject = it.getJSONObject(crateJsonArrayIndex)
            val fieldName = createJsonObject.getString("fieldName")

            var multipleForm = false
            var subFormName = ""

            var subFormViewList = ArrayList<View>()
            if (createJsonObject.has("multiple") && createJsonObject.has("groupName")) {
                subFormName = createJsonObject.getString("groupName")
                if (createJsonObject.getBoolean("multiple")) {
                    multipleForm = true
                } else {
                    multipleForm = false
                }
            } else {
                multipleForm = false
            }

            if (!fieldName.contains("Div")) {
                when (fieldName.toUpperCase(Locale.ROOT)) {


                    "SUBMIT" -> {
                        // val fieldName = childobject.getString("fieldName")
                        val button: Button = mDynamicViewHelper.button(mMainActivity)
                        button.apply {
                            //setTextColor(0xFFFFFF)
                            text = fieldName
                            width = LinearLayout.LayoutParams.MATCH_PARENT
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                setTextColor(
                                    mMainActivity.getColor(
                                        R.color.text_color_black
                                    )
                                )
                                setBackgroundColor(
                                    mMainActivity.getColor(
                                        R.color.text_color_black
                                    )
                                )

                                background = mMainActivity.getDrawable(R.drawable.bg_view)

                            }
                            contentLayout.run { addView(button) }

                            //code for the submit button
                            button.setOnClickListener {
                                when (button.text.toString().toUpperCase(Locale.ROOT)) {

                                    "SUBMIT" -> {
                                        //ubmitDataToServer()
                                        // SubmitDataToServer()

                                        //mValidateAllView()
                                    }

                                    "CANCEL" -> {

                                    }

                                }
                            }

                        }

                    }
                }
            } else {


                val fieldNameconut =
                    createJsonObject.getString("fieldName").replace("Div", "").toInt()

                for (childcount in 0 until fieldNameconut) {
                    val children: JSONArray
                    children = if (childcount == 0) {
                        createJsonObject.getJSONArray("children")
                    } else {
                        createJsonObject.getJSONArray("children${childcount}")
                    }

                    for (childcount1 in 0 until children.length()) {
                        val childobject: JSONObject = children.getJSONObject(childcount1)


                        var strFieldtype = ""

                        if (childobject.has("fieldType")) {
                            strFieldtype = childobject.getString("fieldType")
                        }
                        val questionTitle = childobject.getString("fieldName")
                        val columnID = childobject.getString("columnID")
                        // val viewReferenceId = childobject.getString("columnID")
                        when (strFieldtype.toUpperCase(Locale.ROOT)) {
                            "TEXT" -> {

                                val tvQuestion =
                                    mDynamicViewHelper.text_question(context = mMainActivity)
                                val required = childobject.getBoolean("required")
                                when (required) {
                                    true -> {
                                        val str_question =
                                            mDynamicViewHelper.MandateMark(questionTitle)
                                        tvQuestion.text = str_question
                                        aListofMandateObjectCheck.add(columnID)
                                    }
                                    else -> {
                                        tvQuestion.text = questionTitle
                                    }
                                }


                                if (multipleForm) {
                                    tvQuestion.setTag("Question")
                                    contentSubFormLayout.addView(tvQuestion)
                                    // subFormViewList.add(tvQuestion)
                                } else {
                                    contentLayout.addView(tvQuestion)
                                }


                                val edittext: EditText = mDynamicViewHelper.edittext(mMainActivity)
                                edittext.inputType = InputType.TYPE_CLASS_TEXT

                                //edit form
//                                if (mSelectedUserDetails.has(columnID)){
//                                    val strSelectedUserDetails=mSelectedUserDetails.getString(columnID)
//                                    edittext.setText(strSelectedUserDetails.toString())
//                                }


                                edittext.tag = questionTitle
                                edittext.setLines(2)
                                edittext.setTextColor(Color.BLACK)



                                if (multipleForm) {
                                    contentSubFormLayout.addView(edittext)
                                    // subFormViewList.add(edittext)

                                } else {
                                    contentLayout.addView(edittext)
                                }


                                //for the All View Instance to send to Server and Type
                                allViewInstance.add(edittext)
                                viewTypes.add("TEXT")
                                aListofKeyObject.add(columnID)


                            }
                            "TEXTAREA" -> {
                                val tvQuestion =
                                    mDynamicViewHelper.text_question(context = mMainActivity)
                                val required = childobject.getBoolean("required")
                                when (required) {
                                    true -> {
                                        val str_question =
                                            mDynamicViewHelper.MandateMark(questionTitle)
                                        tvQuestion.text = str_question
                                        aListofMandateObjectCheck.add(columnID)
                                    }
                                    else -> {
                                        tvQuestion.text = questionTitle
                                    }
                                }

                                if (multipleForm) {
                                    tvQuestion.setTag("Question")
                                    contentSubFormLayout.addView(tvQuestion)
                                    //subFormViewList.add(tvQuestion)

                                } else {
                                    contentLayout.addView(tvQuestion)
                                }

                                val edittext: EditText = mDynamicViewHelper.edittext(mMainActivity)

                                edittext.apply {
                                    edittext.inputType = InputType.TYPE_CLASS_TEXT
                                    edittext.tag = questionTitle
                                    isSingleLine = false
                                    inputType =
                                        InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
                                    setLines(5)
                                    maxLines = 5
                                    isVerticalScrollBarEnabled = true
                                    movementMethod =
                                        ScrollingMovementMethod.getInstance()
                                    isFocusable = true
                                    setPadding(16, 0, 16, 0)
                                    setTextColor(Color.BLACK)

                                    //edit Form
//                                    if (mSelectedUserDetails.has(columnID)){
//                                        val strSelectedUserDetails=mSelectedUserDetails.getString(columnID)
//
//                                        if (strSelectedUserDetails.equals("null") || strSelectedUserDetails == null){
//                                            edittext.setText("")
//                                        }else{
//                                            edittext.setText(strSelectedUserDetails.toString())
//                                        }
//                                    }

                                }


                                if (multipleForm) {
                                    contentSubFormLayout.addView(edittext)
                                    // subFormViewList.add(edittext)
                                } else {
                                    contentLayout.addView(edittext)
                                }


                                //for the All View Instance to send to Server and Type
                                allViewInstance.add(edittext)
                                viewTypes.add("TEXTAREA")
                                aListofKeyObject.add(columnID)


                            }
                            "FILE" -> {
                                val tvQuestion =
                                    mDynamicViewHelper.text_question(context = mMainActivity)
                                val required = childobject.getBoolean("required")
                                when (required) {
                                    true -> {
                                        val str_question =
                                            mDynamicViewHelper.MandateMark(questionTitle)
                                        tvQuestion.text = str_question
                                        aListofMandateObjectCheck.add(columnID)
                                    }
                                    else -> {
                                        tvQuestion.text = questionTitle
                                    }
                                }
                                if (multipleForm) {
                                    tvQuestion.setTag("Question")
                                    contentSubFormLayout.addView(tvQuestion)
                                    // subFormViewList.add(tvQuestion)
                                } else {
                                    contentLayout.addView(tvQuestion)
                                }

                                val attachment: LinearLayout = mDynamicViewHelper.attachment(
                                    mMainActivity
                                )

                                val lp = LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT
                                )
                                lp.topMargin = 32
                                lp.marginStart = 16
                                lp.marginEnd = 16

                                val btn = Button(mMainActivity)
                                btn.height = LinearLayout.LayoutParams.WRAP_CONTENT
                                btn.width = LinearLayout.LayoutParams.WRAP_CONTENT
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    btn.background =
                                        mMainActivity.getDrawable(R.drawable.roundwhitebg)
                                }

                                btn.text = "Select"
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    btn.setBackgroundColor(mMainActivity.getColor(R.color.colorPrimary))
                                    btn.setTextColor(mMainActivity.getColor(R.color.text_color_white))
                                }

                                attachment.addView(btn)
                                val edSingle = TextView(mMainActivity)

                                edSingle.isSingleLine = true
                                edSingle.textAlignment =
                                    TextView.TEXT_ALIGNMENT_VIEW_START
                                edSingle.typeface = Typeface.DEFAULT_BOLD
                                edSingle.textSize = 16F
                                edSingle.setTextColor(this.resources.getColor(R.color.text_color_black))
                                edSingle.gravity = Gravity.START or Gravity.CENTER
                                edSingle.layoutParams = lp
                                attachment.addView(edSingle)

                                btn.setOnClickListener {
                                    // call your camera instead of this method
                                    /*  val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                    startActivityForResult(intent, REQUEST_CODE)*/
                                    //mTextViewForImageUpload = ed_single

                                    /* if (checkPermission()) {
                                         //main logic or main code

                                         // . write your main code to execute, It will execute if the permission is already given.
                                         openImagesDialog()
                                     } else {
                                         requestPermission();
                                     }*/


                                }
                                /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                    attachment.setTextColor(this.getColor(R.color.text_color_black))
                                }
                                attachment.apply {
                                    gravity = Gravity.START
                                    text = questionTitle
                                }*/



                                if (multipleForm) {
                                    contentSubFormLayout.addView(attachment)
                                    //  subFormViewList.add(attachment)
                                } else {
                                    contentLayout.addView(attachment)
                                }


                                //for the All View Instance to send to Server and Type
                                allViewInstance.add(attachment)
                                viewTypes.add("FILE")
                                aListofKeyObject.add(columnID)


                            }
                            "DATE" -> {
                                val tvQuestion = mDynamicViewHelper.text_question(mMainActivity)

                                // val textView_question = mViewHelper.text_question(this)
                                when (childobject.getBoolean("required")) {
                                    true -> {
                                        val strQuestion =
                                            mDynamicViewHelper.MandateMark(questionTitle)
                                        tvQuestion.text = strQuestion
                                        aListofMandateObjectCheck.add(columnID)
                                    }
                                    else -> {
                                        tvQuestion.text = questionTitle
                                    }
                                }
                                //tvQuestion.text = questionTitle
                                if (multipleForm) {
                                    tvQuestion.setTag("Question")
                                    contentSubFormLayout.addView(tvQuestion)
                                    // subFormViewList.add(tvQuestion)
                                } else {
                                    contentLayout.addView(tvQuestion)
                                }

                                val dateView: TextView =
                                    mDynamicViewHelper.tv_date_picker(mMainActivity)
                                dateView.hint = childobject.getString("placeholder")
                                dateView.setLines(2)
                                // dateView.setPadding(0,0,0,16)
                                dateView.setTextColor(Color.BLACK)

                                //edit Form
//                                if (mSelectedUserDetails.has(columnID)){
//                                    val strSelectedUserDetails=mSelectedUserDetails.getString(columnID)
//                                    dateView.text=(strSelectedUserDetails.toString())
//                                }
                                dateView.tag = questionTitle


                                if (multipleForm) {
                                    dateView.setTag("DATE")
                                    contentSubFormLayout.addView(dateView)
                                    //subFormViewList.add(dateView)
                                } else {
                                    contentLayout.addView(dateView)
                                }


                                //for the All View Instance to send to Server and type
                                allViewInstance.add(dateView)
                                viewTypes.add("DATE")
                                aListofKeyObject.add(columnID)
                                val c = Calendar.getInstance()
                                val mYear = c.get(Calendar.YEAR)
                                val mMonth = c.get(Calendar.MONTH)
                                val mDay = c.get(Calendar.DAY_OF_MONTH)

                                dateView.text = ("$mYear-${mMonth + 1}-$mDay")

                                dateView.setOnClickListener {

                                    val datePicker = DatePickerDialog(
                                        mMainActivity,
                                        { _, year, month, dayOfMonth ->
                                            dateView.text = String.format(
                                                "%d-%d-%d",
                                                year,
                                                month + 1,
                                                dayOfMonth
                                            )
                                        },
                                        mYear,
                                        mMonth,
                                        mDay
                                    )

                                    datePicker.show()


                                    //global Method To Access Date Picker
//                                             if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                                                 DynamicViewHelper().mDatePicker(GlobalVariables.mMainActity,dateView)
//                                             }
                                }


                            }
                            "RADIO" -> {

                                val displaytype: String =
                                    childobject.getString("displaytype")

                                val tvQuestion =
                                    mDynamicViewHelper.text_question(context = mMainActivity)

                                when (childobject.getBoolean("required")) {
                                    true -> {
                                        val strQuestion =
                                            mDynamicViewHelper.MandateMark(questionTitle)
                                        tvQuestion.text = strQuestion
                                        aListofMandateObjectCheck.add(columnID)
                                    }
                                    else -> {
                                        tvQuestion.text = questionTitle
                                    }
                                }
                                if (multipleForm) {
                                    tvQuestion.setTag("Question")
                                    contentSubFormLayout.addView(tvQuestion)
                                    //subFormViewList.add(tvQuestion)
                                } else {
                                    contentLayout.addView(tvQuestion)
                                }

                                val radioGroup: RadioGroup = mDynamicViewHelper.radio_view(
                                    context = mMainActivity,
                                    message = "YES,NO",
                                    radio_btn_count = 2,
                                    orientation = displaytype
                                )
                                radioGroup.tag = radioGroup.checkedRadioButtonId
                                radioGroup.checkedRadioButtonId


                                if (multipleForm) {
                                    contentSubFormLayout.addView(radioGroup)
                                    //subFormViewList.add(radioGroup)
                                } else {
                                    contentLayout.addView(radioGroup)
                                }


                                //for the All View Instance to send to Server and type
                                allViewInstance.add(radioGroup)
                                viewTypes.add("RADIO")
                                aListofKeyObject.add(columnID)


                            }
                            "NUMBER" -> {
                                val tvQuestion =
                                    mDynamicViewHelper.text_question(context = mMainActivity)
                                val required = childobject.getBoolean("required")
                                when (required) {
                                    true -> {
                                        val str_question =
                                            mDynamicViewHelper.MandateMark(questionTitle)
                                        tvQuestion.text = str_question
                                        aListofMandateObjectCheck.add(columnID)
                                    }
                                    else -> {
                                        tvQuestion.text = questionTitle
                                    }
                                }
                                //tvQuestion.text = questionTitle
                                if (multipleForm) {
                                    tvQuestion.setTag("Question")
                                    contentSubFormLayout.addView(tvQuestion)
                                    // subFormViewList.add(tvQuestion)
                                } else {
                                    contentLayout.addView(tvQuestion)
                                }

                                val edittext: EditText =
                                    mDynamicViewHelper.edittext(context = mMainActivity)
                                edittext.inputType = InputType.TYPE_CLASS_NUMBER
                                //edit Form
//                                if (mSelectedUserDetails.has(columnID)){
//                                    val strSelectedUserDetails=mSelectedUserDetails.getString(columnID)
//                                    edittext.setText(strSelectedUserDetails.toString())
//                                }
                                edittext.tag = questionTitle
                                edittext.setPadding(16, 0, 16, 0)
                                edittext.setLines(2)
                                edittext.setTextColor(Color.BLACK)




                                if (multipleForm) {
                                    contentSubFormLayout.addView(edittext)
                                    //subFormViewList.add(edittext)
                                } else {
                                    contentLayout.addView(edittext)
                                }


                                /* edittext.setOnFocusChangeListener { view, hasFocus ->
                                     if (hasFocus) {
                                         getWindow().setSoftInputMode(
                                             WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
                                         );
                                     }


                                 }*/

                                //for the All View Instance to send to Server and Type
                                allViewInstance.add(edittext)
                                viewTypes.add("NUMBER")
                                aListofKeyObject.add(columnID)

                            }
                            "SELECT" -> {
                                val textView_question = mDynamicViewHelper.text_question(
                                    mMainActivity
                                )

                                val fieldCode = childobject.getString("columnID")
                                val CascadeValue = childobject.getString("CascadeValue")
                                val required = childobject.getBoolean("required")


                                when (required) {
                                    true -> {
                                        val str_question =
                                            mDynamicViewHelper.MandateMark(questionTitle)
                                        textView_question.text = str_question
                                        aListofMandateObjectCheck.add(columnID)
                                    }
                                    else -> {
                                        textView_question.text = questionTitle
                                    }
                                }


                                if (multipleForm) {
                                    textView_question.setTag("Question")
                                    contentSubFormLayout.addView(textView_question)
                                    // subFormViewList.add(textView_question)
                                } else {
                                    contentLayout.addView(textView_question)
                                }


                                // var aList: ArrayList<String> = ArrayList()
                                var spinner: TextView =
                                    mDynamicViewHelper.spinnerTextView(mMainActivity)
                                // aList = clusterLeadDropDown(fieldCode)
                                spinner.text = "--Select--"
                                spinner.textSize = 16F
                                spinner.setLines(2)
                                spinner.setTextColor(Color.BLACK)



                                //edit Form
                                /*  if (mSelectedUserDetails.has(columnID)){
                                    val strSelectedUserDetails=mSelectedUserDetails.getString(columnID)


                                    Log.d("ChildCount",childcount1.toString())

                                    if (!CascadeValue.isEmpty()) {

                                        val CascadedItem=childobject.getJSONArray("CascadedItem").getJSONArray(0).get(0) as String

                                        for (i in 0 until aListofKeyObject.size) {

                                            when (CascadedItem) {
                                                aListofKeyObject.get(i) -> {
                                                    try {


                                                        spinner.tag=strSelectedUserDetails.toString()
                                                        spinner=editedDropDownData(strSelectedUserDetails,fieldCode,spinner,aListofKeyObject.get(i),mSelectedUserDetails.getString(aListofKeyObject.get(i)))

                                                    } catch (e: java.lang.Exception) {

                                                    }

                                                }
                                            }

                                        }

                                    } else {
                                        spinner.tag=strSelectedUserDetails.toString()
                                        spinner=editedDropDownData(strSelectedUserDetails,fieldCode,spinner,"","")

                                    }
                                }*/
                                spinner.setOnClickListener {

                                   /* if (!CascadeValue.isEmpty()) {

                                        val CascadedItem =
                                            childobject.getJSONArray("CascadedItem").getJSONArray(
                                                0
                                            ).get(0) as String

                                        for (i in 0 until aListofKeyObject.size) {


                                            try {
                                                mDropDown(
                                                    fieldCode,
                                                    spinner,
                                                    CascadedItem,
                                                    (allViewInstance.get(i) as TextView).tag.toString(),
                                                    "Select  " + questionTitle
                                                )
                                            } catch (e: java.lang.Exception) {

                                            }



                                           *//* when (CascadedItem) {
                                                aListofKeyObject.get(i) -> {
                                                    try {
                                                        mDropDown(
                                                            fieldCode,
                                                            spinner,
                                                            CascadedItem,
                                                            (allViewInstance.get(i) as TextView).tag.toString(),
                                                            "Select  " + questionTitle
                                                        )
                                                    } catch (e: java.lang.Exception) {

                                                    }

                                                }
                                            }*//*

                                        }

                                    } else {
                                        mDropDown(
                                            fieldCode,
                                            spinner,
                                            CascadeValue,
                                            "",
                                            "Select  " + questionTitle
                                        )
                                    }
*/



                                        mDropDown(
                                            fieldCode,
                                            spinner
                                        )



                                }

                                if (multipleForm) {
                                    spinner.setTag(fieldCode)
                                    contentSubFormLayout.addView(spinner)
                                    //subFormViewList.add(spinner)
                                } else {
                                    contentLayout.addView(spinner)

                                }


                                //val spinner: TextView = mViewHelper.spinnerTextView(this)

                                // contentLayout.addView(spinner)


                                //for the All View Instance to send to Server and type
                                allViewInstance.add(spinner)
                                viewTypes.add("SELECT")
                                aListofKeyObject.add(columnID)


                            }

                        }


                    }

                }
                if (multipleForm) {

                    //used Xml Design Fro Sub-From
                    // val mSubForm= customViewSubFormDesign(contentSubFormLayout, subFormName)

                    val arrayView = ArrayList<View>()
                    for (subFormView in 0 until contentSubFormLayout.childCount) {
                        arrayView.add(contentSubFormLayout.getChildAt(subFormView))
                    }
                    subFormViewList = ArrayList()
                    val subformModelData = arraySubFormDataClas(
                        subFormViewList,
                        contentSubFormLayout.childCount,
                        subFormName,
                        contentSubFormLayout
                    )
                    listSubFormDetails.add(subformModelData)
                    /* val mSubForm = normalKotlinCodeToGenerateUI(
                         contentSubFormLayout,
                         subformModelData,
                         subFormName,
                         llAddform
                     )*/


                    ///////////////////////////////////////

                    var linearLayoutChildSubForm = mDynamicViewHelper.LinearLayoutViewHelper(mMainActivity)

                    //val mSrollView = mDynamicViewHelper.ScrollViewHelper(mMainActivity)

                    val dateView: TextView = mDynamicViewHelper.tv_date_picker(mMainActivity)

                    dateView.apply {

                        val lp = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                        lp.topMargin = 16
                        lp.bottomMargin = 32

                        dateView.layoutParams = lp
                        dateView.gravity = Gravity.CENTER

                        setLines(2)
                        setCompoundDrawablesRelativeWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.ic_baseline_add_24,
                            0
                        )
                        indexSubFormPosition = 0
                        setPadding(0, 0, 0, 16)
                        setTextColor(Color.BLACK)
                        setText(subFormName)

                    }

                    dateView.setOnClickListener {

                        Log.d("ListItemsdwd", listSubFormDetails.get(0).arrayview.size.toString())
                        Log.d("ListItemscount", listSubFormDetails.get(0).conutView.toString())


                        var contentSubFormLayoutchild =
                            mDynamicViewHelper.LinearLayoutViewHelper(mMainActivity)

                        for (view in 0 until listSubFormDetails.size) {

                            if (dateView.text.toString()
                                    .equals(listSubFormDetails.get(view).subFormFlag)
                            ) {
                                Log.d(
                                    "qwedfgf",
                                    listSubFormDetails.get(view).SubForm.childCount.toString()
                                )

//                                if (listSubFormDetails.get(view).SubForm.parent != null) {
//                                    (listSubFormDetails.get(view).SubForm.getParent() as ViewGroup).removeView(
//                                        listSubFormDetails.get(view).SubForm
//                                    )
//                                }
                                contentSubFormLayoutchild = listSubFormDetails.get(view).SubForm


                                Log.d(
                                    "ListItems",
                                    listSubFormDetails.get(view).SubForm.childCount.toString()
                                )

                            }
                        }


                        if (contentSubFormLayoutchild.childCount == 0) {

                            for (view in 0 until listSubFormDetails.size) {

                                if (dateView.text.toString()
                                        .equals(listSubFormDetails.get(view).subFormFlag)
                                ) {
                                    for (countView in 0 until listSubFormDetails.get(view).conutView) {

                                        if (listSubFormDetails.get(view).arrayview.get(countView).parent != null) {
                                            (listSubFormDetails.get(view).arrayview.get(countView).getParent() as ViewGroup).removeView(
                                                listSubFormDetails.get(view).arrayview.get(countView)
                                            )
                                        }


                                        contentSubFormLayoutchild.addView(
                                            listSubFormDetails.get(
                                                view
                                            ).arrayview.get(countView)
                                        )
                                    }
                                }
                            }
                        }

                        for (enteredView in 0 until contentSubFormLayoutchild.childCount) {
                            val mView =
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                    (contentSubFormLayoutchild.getChildAt(enteredView).accessibilityClassName).toString()
                                        .replace("android.widget.", "")
                                } else {
                                    TODO("VERSION.SDK_INT < M")
                                }


                            Log.d("SubFormarr", mView)

                            if (mView.equals("EditText")) {
                                (contentSubFormLayoutchild.getChildAt(enteredView) as EditText).setText(
                                    ""
                                )
                            }

                        }


                        val mScrollView = mDynamicViewHelper.ScrollViewHelper(mMainActivity)
                        mScrollView.addView(contentSubFormLayoutchild)
                        val addDialog = MaterialAlertDialogBuilder(mMainActivity)
                        addDialog.setView(mScrollView)
                        addDialog.setNegativeButton(
                            "cancel",
                            DialogInterface.OnClickListener { dialogInterface, i ->
                                dialogInterface.dismiss()
                            })
                        addDialog.setPositiveButton(
                            "Submit",
                            DialogInterface.OnClickListener { dialogInterface, i ->

                                val conView = llAddform.getChildAt(0) as LinearLayout

                                var contentSubFormLayouttchild = LinearLayout(mMainActivity)
                                contentSubFormLayouttchild.apply {
                                    val mSubFormLayoutlayoutParams = LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                    )
                                    mSubFormLayoutlayoutParams.topMargin = 32
                                    mSubFormLayoutlayoutParams.marginStart = 32
                                    mSubFormLayoutlayoutParams.marginEnd = 32

                                    layoutParams = mSubFormLayoutlayoutParams

                                    orientation = LinearLayout.VERTICAL
                                    gravity = Gravity.CENTER
                                    setPadding(0, 16, 0, 16)
                                }

//                                if (contentSubFormLayouttchild.parent != null) {
//                                    (contentSubFormLayouttchild.getParent() as ViewGroup).removeView(
//                                        contentSubFormLayouttchild
//                                    )
//                                }
//                                if (contentSubFormLayoutchild.parent != null) {
//                                    (contentSubFormLayoutchild.getParent() as ViewGroup).removeView(
//                                        contentSubFormLayoutchild
//                                    )
//                                }

                                /*for (enteredView in 0 until contentSubFormLayouttchild.childCount) {
                                    val mView = (contentSubFormLayouttchild.getChildAt(enteredView).toString()).replace("android.widget.","")

                                    *//*  if (mView.parent != null) {
                                          (mView.getParent() as ViewGroup).removeView(
                                              mView
                                          )
                                      }
                                      contentSubFormLayouttchild.addView(mView)
  *//*
                                    Log.d("SubFormarr", mView.toString())

                                    if (mView.equals("EditText")){
                                        (contentSubFormLayouttchild.getChildAt(enteredView) as EditText).setText("")
                                    }

                                }*/


                                //  contentSubFormLayouttchild=contentSubFormLayoutchild
                                contentSubFormLayouttchild = contentSubFormLayoutchild
                                var arrayAddedFrom = ArrayList<View>()
                                var mDividerItemDecoration=StringBuilder()
                                val arrayDivider=ArrayList<Int>()
                                for (view in 0 until listSubFormDetails.size) {

                                    if (dateView.text.toString()
                                            .equals(listSubFormDetails.get(view).subFormFlag)
                                    ) {

                                        mDividerItemDecoration.append(listSubFormDetails.get(view).conutView)
                                        var mDividerNumber=listSubFormDetails.get(view).conutView

                                        //mDividerNumber=mDividerNumber-2
                                        // Log.d("SubFormarr", contentSubFormLayouttchild.children.toString())

                                        ///////////////////////22.02.21
                                        for (childenterdData in 0 until contentSubFormLayouttchild.childCount) {
                                            val mViewChildEntered =
                                                contentSubFormLayouttchild.getChildAt(
                                                    childenterdData
                                                )

//                                            listSubFormDetails.get(view).arrayview.add(
//                                                mViewChildEntered
//                                            )

//                                            if (mDividerNumber == childenterdData){
//                                                mDividerItemDecoration.append(childenterdData.toString())
//                                                mDividerNumber+=mDividerNumber
//                                            }

                                            val viewType =
                                                mViewChildEntered.accessibilityClassName.toString()
                                                    .replace("android.widget.", "")


                                            when (viewType) {
                                                "TextView" -> {


                                                  val tvView=  (mViewChildEntered as TextView).tag.toString()

                                                    when(tvView){
                                                        "Question"->{
                                                            val tvQuestion =
                                                                mDynamicViewHelper.text_question(
                                                                    context = mMainActivity
                                                                )
                                                            tvQuestion.setText((mViewChildEntered as TextView).text.toString())
                                                            listSubFormDetails.get(view).arrayview.add(
                                                                tvQuestion
                                                            )
                                                        }

                                                        "DATE"->{
                                                            val dateView: TextView =
                                                                mDynamicViewHelper.tv_date_picker(mMainActivity)
                                                            dateView.text = (mViewChildEntered as TextView).text.toString()

                                                            dateView.setLines(2)
                                                            // dateView.setPadding(0,0,0,16)
                                                            dateView.setTextColor(Color.BLACK)

                                                            dateView.setOnClickListener {
                                                                val listSelectedDate=(mViewChildEntered as TextView).text.toString().split("-")

                                                                val datePicker = DatePickerDialog(
                                                                    mMainActivity,
                                                                    { _, year, month, dayOfMonth ->
                                                                        dateView.text = String.format(
                                                                            "%d-%d-%d",
                                                                            year,
                                                                            month + 1,
                                                                            dayOfMonth
                                                                        )
                                                                    },
                                                                    listSelectedDate.get(0).toInt(),
                                                                    listSelectedDate.get(1).toInt(),
                                                                    listSelectedDate.get(2).toInt()
                                                                )

                                                                datePicker.show()


                                                                //global Method To Access Date Picker
//                                             if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                                                 DynamicViewHelper().mDatePicker(GlobalVariables.mMainActity,dateView)
//                                             }
                                                            }

                                                            listSubFormDetails.get(view).arrayview.add(
                                                                dateView
                                                            )
                                                        }
                                                        else->{
                                                            var spinner: TextView =
                                                                mDynamicViewHelper.spinnerTextView(mMainActivity)
                                                            // aList = clusterLeadDropDown(fieldCode)
                                                            spinner.text = (mViewChildEntered as TextView).text.toString()
                                                            spinner.textSize = 16F
                                                            spinner.setLines(2)
                                                            spinner.setTextColor(Color.BLACK)

                                                            spinner.setOnClickListener {
                                                                mDropDown(
                                                                    tvView,
                                                                    spinner
                                                                )
                                                            }

                                                            listSubFormDetails.get(view).arrayview.add(
                                                                spinner
                                                            )
                                                        }

                                                    }

                                                }
                                                "EditText" -> {
                                                    val edittext: EditText =
                                                        mDynamicViewHelper.edittext(
                                                            mMainActivity
                                                        )
                                                    edittext.inputType = InputType.TYPE_CLASS_TEXT
                                                    edittext.setLines(2)
                                                    edittext.setTextColor(Color.BLACK)

                                                    edittext.setText((mViewChildEntered as EditText).text.toString())
                                                    listSubFormDetails.get(view).arrayview.add(
                                                        edittext
                                                    )
                                                }
                                                "LinearLayout" -> {

                                                    val attachment: LinearLayout =
                                                        mDynamicViewHelper.attachment(
                                                            mMainActivity
                                                        )

                                                    val lp = LinearLayout.LayoutParams(
                                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                                        LinearLayout.LayoutParams.MATCH_PARENT
                                                    )
                                                    lp.topMargin = 32
                                                    lp.marginStart = 16
                                                    lp.marginEnd = 16

                                                    val btn = Button(mMainActivity)
                                                    btn.height =
                                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                                    btn.width =
                                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                        btn.background =
                                                            mMainActivity.getDrawable(R.drawable.roundwhitebg)
                                                    }

                                                    btn.text = "Select"
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        btn.setBackgroundColor(
                                                            mMainActivity.getColor(
                                                                R.color.colorPrimary
                                                            )
                                                        )
                                                        btn.setTextColor(mMainActivity.getColor(R.color.text_color_white))
                                                    }

                                                    attachment.addView(btn)
                                                    val edSingle = TextView(mMainActivity)

                                                    edSingle.isSingleLine = true
                                                    edSingle.textAlignment =
                                                        TextView.TEXT_ALIGNMENT_VIEW_START
                                                    edSingle.typeface = Typeface.DEFAULT_BOLD
                                                    edSingle.textSize = 16F
                                                    edSingle.setTextColor(this.resources.getColor(R.color.text_color_black))
                                                    edSingle.gravity =
                                                        Gravity.START or Gravity.CENTER
                                                    edSingle.layoutParams = lp
                                                    attachment.addView(edSingle)

                                                    listSubFormDetails.get(view).arrayview.add(
                                                        attachment
                                                    )


                                                }

                                            }
                                            if (viewType.equals("EditText")) {
                                                Log.d(
                                                    "EditTextSubForm",
                                                    (mViewChildEntered as EditText).text.toString()
                                                )
                                            }

                                        }


                                        /////////////////////////////22.02.21
//                                        listSubFormDetails.get(view).arrayview.add(
//                                            contentSubFormLayouttchild
//                                        )
                                        //arrayAddedFrom = listSubFormDetails.get(view).arrayview


                                        if (contentSubFormLayoutchild.parent != null) {
                                            (contentSubFormLayoutchild.getParent() as ViewGroup).removeView(
                                                contentSubFormLayoutchild
                                            )
                                        }

                                        for (childindex in 0 until listSubFormDetails.get(view).arrayview.size) {


                                            Log.d(
                                                "hjfhbjnkm",
                                                listSubFormDetails.get(view).conutView.toString()
                                            )

                                            //   for (ccc in 0 until listSubFormDetails.get(view).arrayview.get(childindex).childCount) {
//                                            for (ccc in 0 until listSubFormDetails.get(view).conutView) {
//                                                //  val childView = listSubFormDetails.get(view).arrayview.get(childindex).getChildAt(ccc)
//                                                val childView = listSubFormDetails.get(view).arrayview.get(ccc)
//                                                if (childView.parent != null) {
//                                                    (childView.getParent() as ViewGroup).removeView(
//                                                        childView
//                                                    )
//                                                }
//
//                                                contentSubFormLayoutchild.addView(childView)
//
//                                            }

                                            val childView =
                                                listSubFormDetails.get(view).arrayview.get(
                                                    childindex
                                                )
                                            if (childView.parent != null) {
                                                (childView.getParent() as ViewGroup).removeView(
                                                    childView
                                                )
                                            }
                                            // contentSubFormLayoutchild.addView(childView)


                                            if (contentSubFormLayoutchild.parent != null) {
                                                (contentSubFormLayoutchild.getParent() as ViewGroup).removeView(
                                                    contentSubFormLayoutchild
                                                )
                                            }





                                            if (childView.parent != null) {
                                                (childView.getParent() as ViewGroup).removeView(
                                                    childView
                                                )
                                            }
                                            arrayAddedFrom.add(childView)



                                            Log.d("mDividerNumber", (mDividerNumber).toString())
//                                            if (mDividerNumber - 2 == childindex) {
//                                                arrayAddedFrom.add( mDynamicViewHelper.DividerLineViewHelper(mMainActivity))
//                                                mDividerNumber+=mDividerNumber
//                                            }
                                            mDividerNumber= (listSubFormDetails.get(view).conutView) + mDividerNumber
                                            arrayDivider.add(mDividerNumber)

                                            val viewType = childView.accessibilityClassName.toString().replace("android.widget.", "")
                                            // contentLayout.addView(childView)

                                        }
                                    }
                                }

                                Log.d("listSize", arrayAddedFrom.size.toString())


                                arrayDivider.forEachIndexed { index, i ->
                                    if (i < arrayAddedFrom.size){
                                        if (index%2==0){
                                            arrayAddedFrom.add((i-2), mDynamicViewHelper.DividerLineViewHelper(mMainActivity))
                                        }
                                    }
                                }


                                for (whenkfnjnfsn in 0 until arrayAddedFrom.size) {
                                    if (arrayAddedFrom.get(whenkfnjnfsn).parent != null) {
                                        (arrayAddedFrom.get(whenkfnjnfsn)
                                            .getParent() as ViewGroup).removeView(
                                            arrayAddedFrom.get(whenkfnjnfsn)
                                        )
                                    }




                                    linearLayoutChildSubForm.addView(
                                        arrayAddedFrom.get(
                                            whenkfnjnfsn
                                        )
                                    )

//                                    Log.d("mDividerItemDecoration", mDividerItemDecoration.toString())
//                                    if (mDividerItemDecoration.contains(whenkfnjnfsn.toString())){
//                                        linearLayoutChildSubForm.addView(mDynamicViewHelper.DividerLineViewHelper(
//                                            mMainActivity))
//                                    }





                                    val viewType = arrayAddedFrom.get(whenkfnjnfsn).accessibilityClassName.toString().replace("android.widget.", "")

                                    Log.d("listviewType", viewType.toString())

                                }



                                /*  for (childindex in 0 until arrayAddedFrom.size){

                                      for (ccc in 0 until  (arrayAddedFrom.get(childindex) as LinearLayout).childCount){
                                          val childView=(arrayAddedFrom.get(childindex) as LinearLayout).getChildAt(ccc)
                                          if (childView.parent != null) {
                                              ( childView.getParent() as ViewGroup).removeView(childView)
                                          }

                                          Log.d("hjfhbjnkm", ccc.toString())
                                          contentSubFormLayoutchild.addView(childView)
                                      }
                                  }*/


                                /* for (childView in 0 until contentSubFormLayoutchild.childCount) {
                                     if (contentSubFormLayoutchild.getChildAt(childView).parent != null) {
                                         (contentSubFormLayoutchild.getChildAt(childView)
                                             .getParent() as ViewGroup).removeView(
                                             contentSubFormLayoutchild.getChildAt(childView)
                                         )
                                     }
                                     contentSubFormLayouttchild.addView(
                                         contentSubFormLayoutchild.getChildAt(
                                             childView
                                         )
                                     )
                                 }*/
////////////////////////////////////////////////////////
                                /* for (indcon in 0 until conView.childCount){
                                     val eachView=conView.getChildAt(indcon)
                                     if (eachView.tag != null){
                                         if (eachView.tag.toString().equals("SubForm")){
                                             val induvtualView=eachView as LinearLayout
                                             //induvtualView.addView(linearLayoutChildSubForm,induvtualView.childCount-1)

                                             for (view in 0 until listSubFormDetails.size) {

                                                 if (dateView.text.toString()
                                                         .equals(listSubFormDetails.get(view).subFormFlag)
                                                 ) {
                                                     listSubFormDetails.get(view).arrayview.forEach {
                                                         // llAddform.addView(it,1)
                                                         if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                                             Log.d("ViewType",it.accessibilityClassName.toString())
                                                             val viewType=  it.accessibilityClassName.toString().replace("android.widget.","")
                                                             if (!viewType.equals("LinearLayout")){
                                                                 if (it.parent != null) {
                                                                     (it.getParent() as ViewGroup).removeView(it)
                                                                 }
                                                                 contentSubFormLayoutchild.addView(it)
                                                                 Log.d("ViewTypewithoutLinear",it.toString())

                                                             }

                                                         } else {

                                                         }

                                                     }

                                                 }
                                             }
                                         }
                                     }
                                 }*/

                                //////////////////////////////////////////////////////////
                                // if (linearLayoutChildSubForm.parent != null) {
                                //   (linearLayoutChildSubForm.getParent() as ViewGroup).removeView(linearLayoutChildSubForm)
                                // }
//                        linearLayoutChildSubForm.addView(dateView)
                                Log.d(
                                    "ViewTypewithoutLinearrr",
                                    contentSubFormLayouttchild.childCount.toString()
                                )

                                //  linearLayoutChildSubForm.addView( contentSubFormLayoutchild)
                                //linearLayoutChildSubForm.addView(contentSubFormLayouttchild)

                                dialogInterface.dismiss()
                            })

                            .show()


                        /* val subFormChildName=dateView.text.toString()

                         //mArrayListMultipleSubForm.add(contentSubFormLayout)
                         //alertSubForm(contentSubFormLayout, linearLayoutChildSubForm)
                         //alertSubForm(contentSubFormLayout, linearLayoutChildSubForm,mSrollView)
                         //alertSubForm(contentSubFormLayout, mSrollView)

                         if (linearLayoutChildSubForm.parent != null) {
                             (linearLayoutChildSubForm.getParent() as ViewGroup).removeView(linearLayoutChildSubForm)
                         }
                         var index=0

                        // linearLayoutChildSubForm.addView(dateView,index)
                         for (view in 0 until listSubFormDetails.size) {

                             if (dateView.text.toString()
                                     .equals(listSubFormDetails.get(view).subFormFlag)
                             ) {
                                 listSubFormDetails.get(view).arrayview.forEach {
                                     linearLayoutChildSubForm.addView(it,index)
                                 }

                             }
                         }*/

                    }
                    contentLayout.addView(dateView)


                    /////////////////////////////////////////
                    //  contentLayout.addView(contentSubFormLayout)
                    // contentLayout.addView(mSrollView)

                    // contentSubFormLayout.addView(tvQuestion)
                    linearLayoutChildSubForm.tag = "SubForm"


                    allViewInstance.add(linearLayoutChildSubForm)
                    contentLayout.addView(linearLayoutChildSubForm)
                }


            }

        }




        llAddform.addView(contentLayout)


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun normalKotlinCodeToGenerateUI(
        contentSubFormLayout: LinearLayout,
        subformModelData: arraySubFormDataClas,
        subFormName: String,
        llAddform: LinearLayout
    ): LinearLayout {
        val table = TableLayout(mMainActivity)

        table.isStretchAllColumns = true
        table.isShrinkAllColumns = true

        var linearLayoutChildSubForm = mDynamicViewHelper.LinearLayoutViewHelper(mMainActivity)

        //val mSrollView = mDynamicViewHelper.ScrollViewHelper(mMainActivity)

        val dateView: TextView = mDynamicViewHelper.tv_date_picker(mMainActivity)


//        linearLayoutChildSubForm.setOnClickListener {
//            MaterialAlertDialogBuilder(mMainActivity).setView(contentSubFormLayout)
//                .setPositiveButton(
//                    "Submit",
//                    DialogInterface.OnClickListener { dialogInterface, i ->
//
//                        //linearLayoutChildSubForm.addView(text_question(mMainActivity))
//
//
//                        // linearLayoutChildSubForm.addView(contentSubFormLayout)
//
////                            for (allViewIndex in 0 until allViewInstance.size){
////                                val eachView=allViewInstance.get(allViewIndex)
////                                if (eachView.tag != null){
////                                    if (eachView.tag.toString().equals("SubForm")){
////                                        val subformView=eachView as LinearLayout
////                                        if ((subformView.getChildAt(0) as TextView).text.toString().equals(subFormName)){
////
////                                        }
////                                    }
////                                }
////                            }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
////                        if (linearLayoutChildSubForm.parent != null) {
////                            (linearLayoutChildSubForm.getParent() as ViewGroup).removeView(linearLayoutChildSubForm)
////                        }
////                        var index=0
////                          linearLayoutChildSubForm.addView(dateView,index)
////                        for (view in 0 until listSubFormDetails.size) {
////
////                            if (dateView.text.toString()
////                                    .equals(listSubFormDetails.get(view).subFormFlag)
////                            ) {
////                                listSubFormDetails.get(view).arrayview.forEach {
////                                    index+=1
////                                    linearLayoutChildSubForm.addView(it,index)
////                                }
////
////                            }
////                        }
//
//                    })
//                .show()
//        }
//

        dateView.apply {

            val lp = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            lp.topMargin = 16
            lp.bottomMargin = 32

            dateView.layoutParams = lp
            dateView.gravity = Gravity.CENTER

            setLines(2)
            setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_baseline_add_24,
                0
            )
            indexSubFormPosition = 0
            setPadding(0, 0, 0, 16)
            setTextColor(Color.BLACK)
            setText(subFormName)
            setOnClickListener {


                MaterialAlertDialogBuilder(mMainActivity).setView(contentSubFormLayout)
                    .setNegativeButton(
                        "cancel",
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                        })
                    .setPositiveButton(
                        "Submit",
                        DialogInterface.OnClickListener { dialogInterface, i ->

                            val conView = llAddform.getChildAt(0) as LinearLayout

                            val contentSubFormLayoutchild = LinearLayout(mMainActivity)
                            contentSubFormLayoutchild.apply {
                                val mSubFormLayoutlayoutParams = LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                                )
                                mSubFormLayoutlayoutParams.topMargin = 32
                                mSubFormLayoutlayoutParams.marginStart = 32
                                mSubFormLayoutlayoutParams.marginEnd = 32

                                layoutParams = mSubFormLayoutlayoutParams

                                orientation = LinearLayout.VERTICAL
                                gravity = Gravity.CENTER
                                setPadding(0, 16, 0, 16)
                            }

                            if (contentSubFormLayoutchild.parent != null) {
                                (contentSubFormLayoutchild.getParent() as ViewGroup).removeView(
                                    contentSubFormLayoutchild
                                )
                            }

                            //  for (indcon in 0 until conView.childCount) {
                            val eachView = conView.getChildAt(0)
                            if (eachView.tag != null) {
                                if (eachView.tag.toString().equals("SubForm")) {
                                    val induvtualView = eachView as LinearLayout
                                    //induvtualView.addView(linearLayoutChildSubForm,induvtualView.childCount-1)

                                    for (view in 0 until listSubFormDetails.size) {

                                        if (dateView.text.toString()
                                                .equals(listSubFormDetails.get(view).subFormFlag)
                                        ) {
                                            listSubFormDetails.get(view).arrayview.forEach {
                                                // llAddform.addView(it,1)
                                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                                    Log.d(
                                                        "ViewType",
                                                        it.accessibilityClassName.toString()
                                                    )
                                                    val viewType =
                                                        it.accessibilityClassName.toString()
                                                            .replace("android.widget.", "")
                                                    if (!viewType.equals("LinearLayout")) {
                                                        if (it.parent != null) {
                                                            (it.getParent() as ViewGroup).removeView(
                                                                it
                                                            )
                                                        }
                                                        contentSubFormLayoutchild.addView(it)
                                                        Log.d(
                                                            "ViewTypewithoutLinear",
                                                            it.toString()
                                                        )

                                                    }

                                                } else {

                                                }

                                            }

                                        }
                                    }
                                }
                            }
                            //   }
                            // if (linearLayoutChildSubForm.parent != null) {
                            //   (linearLayoutChildSubForm.getParent() as ViewGroup).removeView(linearLayoutChildSubForm)
                            // }
//                        linearLayoutChildSubForm.addView(dateView)
                            Log.d(
                                "ViewTypewithoutLinearrr",
                                contentSubFormLayoutchild.childCount.toString()
                            )

                            linearLayoutChildSubForm.addView(contentSubFormLayoutchild)

                            dialogInterface.dismiss()
                        })

                    .show()


                /* val subFormChildName=dateView.text.toString()

                 //mArrayListMultipleSubForm.add(contentSubFormLayout)
                 //alertSubForm(contentSubFormLayout, linearLayoutChildSubForm)
                 //alertSubForm(contentSubFormLayout, linearLayoutChildSubForm,mSrollView)
                 //alertSubForm(contentSubFormLayout, mSrollView)

                 if (linearLayoutChildSubForm.parent != null) {
                     (linearLayoutChildSubForm.getParent() as ViewGroup).removeView(linearLayoutChildSubForm)
                 }
                 var index=0

                // linearLayoutChildSubForm.addView(dateView,index)
                 for (view in 0 until listSubFormDetails.size) {

                     if (dateView.text.toString()
                             .equals(listSubFormDetails.get(view).subFormFlag)
                     ) {
                         listSubFormDetails.get(view).arrayview.forEach {
                             linearLayoutChildSubForm.addView(it,index)
                         }

                     }
                 }*/

            }
        }
        linearLayoutChildSubForm.addView(dateView)

        //  mSrollView.addView(linearLayoutChildSubForm)

        return linearLayoutChildSubForm
    }

    private fun customViewSubFormDesign(
        contentSubFormLayout: LinearLayout,
        subFormName: String
    ): View {
        val inflater = LayoutInflater.from(mMainActivity)
        val inflatedLayout = inflater.inflate(
            R.layout.ll_subform_datalist,
            null
        )

        val llSubForm =
            inflatedLayout.findViewById<LinearLayout>(R.id.ll_details_task_details_layout)
        val iv_addsubform = inflatedLayout.findViewById<ImageView>(R.id.iv_addsubformlist)
        val tv_subformname = inflatedLayout.findViewById<TextView>(R.id.tv_subformname)

        tv_subformname.setText(subFormName)

        llSubForm.tag = subFormName

        if (llSubForm.getParent() != null) {
            (llSubForm.getParent() as ViewGroup).removeView(llSubForm) // <- fix
        }
        //contentSubFormLayout

        iv_addsubform.setOnClickListener {
            // alertSubForm(contentSubFormLayout, llSubForm)


            MaterialAlertDialogBuilder(mMainActivity).setView(contentSubFormLayout)
                .setPositiveButton("Submit", DialogInterface.OnClickListener { dialogInterface, i ->
//                    if (linearLayoutChildSubForm.parent != null) {
//                        (linearLayoutChildSubForm.getParent() as ViewGroup).removeAllViews()
//                    }
                    //  linearLayoutChildSubForm.addView(dateView)

                    val inflater = LayoutInflater.from(mMainActivity)
                        .inflate(R.layout.ll_subformchildlayout, null)
                    val llSubChildForm =
                        (inflater.findViewById(R.id.ll_subformchild) as LinearLayout)

                    for (view in 0 until listSubFormDetails.size) {


                        if (tv_subformname.text.toString().equals(listSubFormDetails.get(view).subFormFlag)) {
                            listSubFormDetails.get(view).arrayview.forEach {
                                llSubChildForm.addView(it)
                            }

                        }
                    }

                    llSubForm.addView(llSubChildForm)
                })
                .show()

        }

        // llSubForm.addView(contentSubFormLayout)

        return inflatedLayout

    }

    private fun alertSubForm(
        contentSubFormLayout: LinearLayout,
        llSubForm: LinearLayout
    ): LinearLayout? {

//        if (contentSubFormLayout.getParent() != null) {
//            (contentSubFormLayout.getParent() as ViewGroup).removeView(contentSubFormLayout) // <- fix
//        }


        MaterialAlertDialogBuilder(mMainActivity).setView(contentSubFormLayout)
//            .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialogInterface, i ->
//                dialogInterface.dismiss()
//            })
            .setPositiveButton("Submit", DialogInterface.OnClickListener { dialogInterface, i ->


//                if (contentSubFormLayout.getParent() != null) {
////                    var count=0
////                    if (llSubScrollForm.childCount ==0){
////                        count=0
////                    }else{
////                        count=llSubScrollForm.childCount
////                    }
//                    (contentSubFormLayout.getParent() as ViewGroup).removeView(contentSubFormLayout) // <- fix
//                }

                //  val arrayViewSubForm=ArrayList<View>()

//                if (llSubForm.getParent() != null) {
//                    if (llSubForm.childCount != 0){
//                        for (i in 0 until llSubForm.childCount){
//                            arrayViewSubForm.add(llSubForm.get(i))
//                        }
//                    }
//                    (llSubForm.getParent() as ViewGroup).removeAllViews()
//                }

                contentSubFormLayout.background =
                    mMainActivity.resources.getDrawable(R.drawable.roundwhitebg)
                //    llSubForm.addView(contentSubFormLayout,indexSubFormPosition)
                //  val linearLayoutChildSubForm = mDynamicViewHelper.LinearLayoutViewHelper(mMainActivity)

                //linearLayoutChildSubForm.addView(contentSubFormLayout)
                // arrayViewSubForm.add(contentSubFormLayout)
//                for (j in 0 until contentSubFormLayout.childCount){
//                    llSubForm.addView(contentSubFormLayout.get(j))
//                }

//                arrayViewSubForm.forEach {
//                    llSubForm.addView(it)
//                }


//                mArrayListMultipleSubForm.forEach {

//                }

                llSubForm.addView(contentSubFormLayout)


            })
            .show()

        return llSubForm

    }

    private fun mSubFormView(contentSubFormLayout: LinearLayout): View {
        val inflater = LayoutInflater.from(mMainActivity)
        val inflatedLayout = inflater.inflate(
            R.layout.ll_subform_datalist,
            null,
            false
        )

        val llSubForm =
            inflatedLayout.findViewById<LinearLayout>(R.id.ll_details_task_details_layout)

        if (llSubForm.getParent() != null) {
            (llSubForm.getParent() as ViewGroup).removeView(llSubForm) // <- fix
        }

        llSubForm.addView(contentSubFormLayout)
        return inflatedLayout
    }

    private fun mDropDown(
        fieldCode: String,
        spinner: TextView
    ) {

        val addViewModel = ViewModelProvider(this).get(AddFormViewModel::class.java)
        addViewModel.getAddFormDropDownDetailsFromServer("Cli20",fieldCode).observe(mMainActivity,
            androidx.lifecycle.Observer {
            if (it != null) {

                getSelectedFieldDropDownValue(it,spinner)



            }
        })
    }



    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun text_question(context: Context): TextView {


        var ed_single = TextView(context)
        var lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        lp.gravity = Gravity.CENTER
        lp.topMargin = 32
        lp.marginStart = 16

        ed_single.isSingleLine = true
        ed_single.textAlignment = TextView.TEXT_ALIGNMENT_VIEW_START
        ed_single.typeface = Typeface.DEFAULT_BOLD
        ed_single.textSize = 16F
        ed_single.text = "dfcgvbhjnkml,;.'"
        ed_single.setTextColor(context.resources.getColor(R.color.colorPrimaryDark))
        ed_single.gravity = Gravity.START or Gravity.CENTER


        ed_single.layoutParams = lp

        return ed_single
    }


    private fun getSelectedFieldDropDownValue(input: JSONArray, spinner: TextView){

        var arraylist_label: ArrayList<String> = ArrayList()
        var arraylist_Value: ArrayList<String> = ArrayList()
        arraylist_label.clear()


        for (i in 0 until input.length()) {
            val eachObject = input.getJSONObject(i)

            val lable = eachObject.getString("Label")
            val value = eachObject.getString("Value")

            arraylist_label.add(lable)
            arraylist_Value.add(value)

        }


        val arrayadpter: ArrayAdapter<String> =
            ArrayAdapter<String>(
                mMainActivity,
                android.R.layout.simple_expandable_list_item_1,
                arraylist_label
            )

        MaterialAlertDialogBuilder(mMainActivity)
            .setSingleChoiceItems(
                arrayadpter,
                0,
                { dialogInterface: DialogInterface, position: Int ->
                    spinner.text=arraylist_label.get(position)
                    spinner.tag=arraylist_Value.get(position)
                    dialogInterface.dismiss()
                    arraylist_label = ArrayList()
                    arraylist_Value = ArrayList()

                })
            .show()


    }


}