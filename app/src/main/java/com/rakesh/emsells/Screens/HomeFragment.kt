package com.rakesh.emsells.Screens

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.rakesh.emsells.MainActivity.Companion.mKeyHomeListArray
import com.rakesh.emsells.MainActivity.Companion.mMainActivity
import com.rakesh.emsells.MainActivity.Companion.navController
import com.rakesh.emsells.R
import com.rakesh.emsells.ViewModels.HomeViewModel
import kotlinx.android.synthetic.main.fragment_first.view.*
import org.json.JSONObject

class HomeFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_first, container, false)

        mView.fab_addfrom.setOnClickListener {
            navController.navigate(R.id.action_FirstFragment_to_SecondFragment)
        }

      setUpRecyclerView()
       // getListDataFromServer()

        return mView
    }

    private fun setUpRecyclerView() {

        val mViewModel=ViewModelProvider(mMainActivity).get(HomeViewModel::class.java)
        mViewModel.getHomeListDataFromServer().observe(mMainActivity){
            if (it!= null){

                val dataObject=it.getJSONArray("data")

                var mObjectToList=JSONObject()
                var arrayObject=ArrayList<JSONObject>()


                var keyObject = ArrayList<String>()
                var datakeyObject = ArrayList<String>()


                     mObjectToList=JSONObject()
                Log.d("mKeyHomeListArray",mKeyHomeListArray.toString())

                val listArrayEmpList=ArrayList<HashMap<String,String>>()
                    for (indData in 0 until dataObject.length()){
                        val indObject=dataObject.getJSONObject(indData) as JSONObject

                        val kjhbhnjm = indObject.toString().replace("{","").replace("}","").split(",").toList()

                        val mHashMapData=HashMap<String,String>()

                        mKeyHomeListArray.forEach {

//                            keyObject = ArrayList<String>()
//                             datakeyObject = ArrayList<String>()

//                            val keyList = it.split(":")
//                            for (check in 0 until keyList.size) {
//                                if (check % 2 == 0) {
//                                    keyObject.add(keyList.get(check))
//                                } else {
//                                    datakeyObject.add(keyList.get(check))
//                                }
//                            }
                            if (it.isNotEmpty()){
                                val mKey=it.substring(1, it.length - 1)
                                if (indObject.has(mKey)){
                                    mHashMapData.put(it,indObject.getString(mKey))
                                }
                            }


                        }

                        listArrayEmpList.add(mHashMapData)

                    }






                Log.d("listArrayEmpList",listArrayEmpList.toString())
                Log.d("dataobject",datakeyObject.toString())

                var jsonObject=HashMap<String,String>()
                var arrayJsonObject=ArrayList<HashMap<String,String>>()

                var indexforchild=0


             /*   for (i in 0 until 5){

                    jsonObject= HashMap<String,String>()
                    mKeyHomeListArray.forEachIndexed { index, s ->

                       // jsonObject.put(s,datakeyObject.get(indexforchild))
                        jsonObject.put(keyObject.get(indexforchild),datakeyObject.get(indexforchild))

                        indexforchild++

                        return@forEachIndexed

                    }
                    arrayJsonObject.add(jsonObject)

                }*/

                Log.d("arrayJsonObject",arrayJsonObject.toString())
                Log.d("jsonObject",jsonObject.toString())
               // Log.d("datajkhkgkjh",datakeyObject.toString())

               /* for (i in 0..datakeyObject.size){


                }*/



//                val hash=HashMap<String,String>(indObject.toString())
//                if (indObject.has(mKeyHomeListArray.get(index))){
//                    val indutualObject=indObject.getString(s)
//                    mObjectToList.put(s,indutualObject)
//                    arrayObject.add(mObjectToList)
//                }


//                val mHomeListAdpter=HomeListAdapter(mMainActivity,it.getJSONArray("data").getJSONObject(0))
//                rv_homelist.adapter=mHomeListAdpter

            }
        }

    }


    fun getListDataFromServer() {

        //val string="\"ContactFF8WT\": \"Contact First Name\",\"ContactL4bd2\": \"Contact Last Name\",\"ContactT8N0P\": \"Contact Title\",\"ContactNpVsi\": \"Contact Number\",\"ContactExCHa\": \"Contact Email\",\"ContactNnV24\": \"Contact Notes\""
        val string = "\"CompanyNibQq\": \"Company Name\",\n" +
                "            \"WebsiteuERO\": \"Website\",\n" +
                "            \"CompanyPSoQQ\": \"Company Phone No\",\n" +
                "            \"AddressVZeh\": \"Address\",\n" +
                "            \"CountryXMNF\": \"Country\",\n" +
                "            \"StateCT7H\": \"State\",\n" +
                "            \"Citye36W\": \"City\",\n" +
                "            \"ZIPcode4L08\": \"ZIP code\",\n" +
                "            \"FaxNumbe6KPd\": \"Fax Number\",\n" +
                "            \"IndustryT9lZ\": \"Industry\",\n" +
                "            \"NotesmTNE\": \"Notes\",\n" +
                "            \"Div4HpN9\": \"Company_Contact\""
        val kjhbhnjm = string.split(",").toList()
        val keyObject = ArrayList<String>()
        val dataObject = ArrayList<String>()
        kjhbhnjm.forEach {
            val keyList = it.split(":")
            for (check in 0 until keyList.size) {

                if (check % 2 == 0) {
                    keyObject.add(keyList.get(check))
                } else {
                    dataObject.add(keyList.get(check))

                }

            }
        }

        Log.d("xdfcgvhbjnkm", keyObject.toString())
        Log.d("dfgvhbjnm", dataObject.toString())
    }


}