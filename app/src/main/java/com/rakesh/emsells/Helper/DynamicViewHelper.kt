package com.rakesh.emsells.Helper

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.LinearLayout
import android.widget.TableLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.LinearLayoutCompat
import com.rakesh.emsells.R
import java.util.*
import kotlin.collections.ArrayList


open class DynamicViewHelper {


    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun edittext(context: Context): EditText {
        val edt_MultiText = EditText(context)
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
            lp.setMargins(8, 8, 8, 0)
//        lp.topMargin = 32
//        lp.marginStart = 16
//        lp.marginEnd=16

        edt_MultiText.layoutParams=lp
        //edt_MultiText.setPadding(8,0,8,0)
        edt_MultiText.setTextColor(R.color.text_color_black)

        edt_MultiText.setPadding(16, 0, 16, 0)


        edt_MultiText.setBackgroundResource(R.drawable.roundwhitebg)


        // edt_MultiText.isSingleLine = true
        //edt_MultiText.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE

        // edt_MultiText.isSingleLine = false
       // edt_MultiText.imeOptions = EditorInfo.IME_FLAG_NO_ENTER_ACTION
       /* edt_MultiText.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE*/
        //edt_MultiText.setLines(5)
       // edt_MultiText.maxLines = 5
        //edt_MultiText.isVerticalScrollBarEnabled = true
        //edt_MultiText.movementMethod = ScrollingMovementMethod.getInstance()
       // edt_MultiText.scrollBarStyle = View.SCROLLBARS_INSIDE_INSET
        //edt_MultiText.isCursorVisible=true
        //edt_MultiText.isFocusable=true


      /*  val llnotes = LinearLayout(context)
        llnotes.orientation = LinearLayout.VERTICAL
        llnotes.addView(edt_MultiText)
*/

// et.setKeepScreenOn();
        // et.setKeepScreenOn();


        //To give padding for the dynamic view
        //To give padding for the dynamic view
      /*  val scale: Float = getResources().getDisplayMetrics().density
        val padding_5dp = (5 * scale + 0.5f).toInt()
        val padding_10dp = (10 * scale + 0.5f).toInt()
        val padding_20dp = (20 * scale + 0.5f).toInt()
        val padding_50dp = (50 * scale + 0.5f).toInt()*/

        return edt_MultiText
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun tv_date_picker(context: Context): TextView {

        val dateText = TextView(context)
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        lp.topMargin = 16
        lp.marginStart = 16
        lp.marginEnd=16

        dateText.textSize = 16F
        dateText.setPadding(8, 24, 24, 8)
        dateText.background = context.resources.getDrawable(R.drawable.roundwhitebg)
        dateText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_date_pick, 0)
        dateText.layoutParams = lp

        return dateText

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun spinner_view(context: Context): Spinner {
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        lp.topMargin = 32
        lp.marginStart = 16
        lp.marginEnd=16
        val spinner_vie: Spinner = Spinner(context)
        spinner_vie.layoutParams = lp
        spinner_vie.background = context.resources.getDrawable(R.drawable.roundwhitebg)
        return spinner_vie
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun spinnerTextView(context: Context): TextView {
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        lp.topMargin = 32
        lp.marginStart = 16
        lp.marginEnd=16
        val spinner_vie: TextView = TextView(context)
        spinner_vie.layoutParams = lp
        //.textSize=24F
        spinner_vie.setPadding(8, 24, 24, 8)
        spinner_vie.setCompoundDrawablesWithIntrinsicBounds(
            0,
            0,
            R.drawable.ic_arrow_drop_down_black_24dp,
            0
        )
        spinner_vie.textAlignment=TextView.TEXT_ALIGNMENT_TEXT_START
        spinner_vie.background = context.resources.getDrawable(R.drawable.roundwhitebg)
        return spinner_vie
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun attachment(context: Context): LinearLayout {
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        lp.topMargin = 32
        lp.marginStart = 16
        lp.marginEnd=16
        val linearLayout=LinearLayout(context)
        linearLayout.layoutParams=lp
        linearLayout.background=context.resources.getDrawable(R.drawable.roundwhitebg)
        linearLayout.orientation=LinearLayout.HORIZONTAL

       /* val btn=Button(context)
        btn.height=LinearLayout.LayoutParams.WRAP_CONTENT
        btn.width=LinearLayout.LayoutParams.WRAP_CONTENT
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            btn.background=context.getDrawable(R.drawable.btn_corner_balck)
        }

        btn.text="Select"
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            btn.setBackgroundColor(context.getColor(R.color.colorPrimary))
            btn.setTextColor(context.getColor(R.color.text_color_white))
        }

        linearLayout.addView(btn)
        var ed_single = TextView(context)

        ed_single.setSingleLine(true)
        ed_single.textAlignment = TextView.TEXT_ALIGNMENT_VIEW_START
        ed_single.typeface = Typeface.DEFAULT_BOLD
        ed_single.setTextSize(16F)
        ed_single.setTextColor(context.resources.getColor(R.color.text_color_black))
        ed_single.gravity = Gravity.START or Gravity.CENTER
        ed_single.layoutParams = lp
        linearLayout.addView(ed_single)
*/
        return linearLayout
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun text_question(context: Context): TextView {


        var ed_single = TextView(context)
        var lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        lp.gravity = Gravity.CENTER
        lp.topMargin = 32
        lp.marginStart = 16

        ed_single.isSingleLine = true
        ed_single.textAlignment = TextView.TEXT_ALIGNMENT_VIEW_START
        ed_single.typeface = Typeface.DEFAULT_BOLD
        ed_single.textSize = 16F
        ed_single.setTextColor(context.resources.getColor(R.color.text_color_black))
        ed_single.gravity = Gravity.START or Gravity.CENTER


        ed_single.layoutParams = lp

        return ed_single
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun radio_view(
        context: Context,
        message: String,
        radio_btn_count: Int,
        orientation: String
    ): RadioGroup {


        val str: String = message //"string,with,comma";

        val aValue:ArrayList<String> = ArrayList()
        aValue.add("1")
        aValue.add("2")


        val aList: java.util.ArrayList<*> =
            java.util.ArrayList<Any?>(Arrays.asList(*str.split(",").toTypedArray()))

        var lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        lp.gravity = Gravity.CENTER_HORIZONTAL
        lp.topMargin = 32
        lp.marginStart = 16


        val rb = arrayOfNulls<RadioButton>(radio_btn_count)
        val rg = RadioGroup(context)

        when(orientation){
            "horizontal" -> {
                rg.orientation = RadioGroup.HORIZONTAL
            }
            "vertical" -> {
                rg.orientation = RadioGroup.VERTICAL
            }
            else ->{
                rg.orientation = RadioGroup.HORIZONTAL
            }
        }

       // rg.orientation = RadioGroup.HORIZONTAL

        for (i in 0 until radio_btn_count) {
            rb[i] = RadioButton(context)
            rb[i]!!.id = i
            rg.addView(rb[i])
            rb[i]!!.tag = aValue[i].toString()
            rb[i]!!.text = aList.get(i).toString()
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                rb[i]!!.setTextColor(context.getColor(R.color.colorPrimaryDark))
            }
            rb[i]!!.highlightColor = context.resources.getColor(R.color.colorPrimaryDark)
        }

        rg.layoutParams = lp
        return rg

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun check_box_view(
        context: Context,
        check_count: Int,
        check_message: String
    ): ArrayList<CheckBox> {
        val CheckBoxcount: Int = check_count
        val checkBoxes_list = ArrayList<CheckBox>()

        var lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        lp.gravity = Gravity.CENTER_HORIZONTAL
        lp.topMargin = 32
        lp.marginStart = 16



        for (i in 0 until CheckBoxcount) {


            val aList: ArrayList<*> = ArrayList<Any?>(
                Arrays.asList<String>(
                    *check_message.split(",").toTypedArray()
                )
            )

            val check1 = CheckBox(context)
            check1.layoutParams = ActionBar.LayoutParams(
                LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            check1.setTextColor(Color.BLACK)
            check1.gravity = Gravity.START
            check1.text = aList.get(i).toString()
            check1.layoutParams = lp
            checkBoxes_list.add(check1)
        }

        return checkBoxes_list

    }



    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    open fun button(context: Context):Button{
        val btnBoutton=Button(context)
        val layoutParams=LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.topMargin = 32
        layoutParams.marginStart = 16
        btnBoutton.textSize = 16F

        btnBoutton.layoutParams=layoutParams
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            btnBoutton.setTextColor(context.getColor(R.color.text_color_white))
            btnBoutton.background=context.getDrawable(R.drawable.bg_view)
        }


        return  btnBoutton
    }

    open fun MandateMark(message: String): SpannableStringBuilder {
        //Mandatory mark
        val colored = "* "

        val builder_star = SpannableStringBuilder()

        builder_star.append(message.replace("*", ""))
        val start = builder_star.length
        builder_star.append(colored)
        val end = builder_star.length
        builder_star.setSpan(
            ForegroundColorSpan(Color.RED), start, end,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return builder_star
    }


    @RequiresApi(Build.VERSION_CODES.N)
    open fun mDatePicker(context: Context, mView: TextView){


        val c=Calendar.getInstance()
        val mYear=c.get(Calendar.YEAR)
        val mMonth=c.get(Calendar.MONTH)
        val mDay=c.get(Calendar.DAY_OF_MONTH)
        val datePicker = DatePickerDialog(
            context,
            { _, year, month, dayOfMonth ->
                mView.text = String.format(
                    "%d-%d-%d",
                    year,
                    month + 1,
                    dayOfMonth
                )
            },
            mYear,
            mMonth,
            mDay
        )

        datePicker.show()

    }


   open fun ScrollViewHelper(context: Context):ScrollView{
       val scrollView = ScrollView(context)
       val layoutParams = LinearLayout.LayoutParams(
           ViewGroup.LayoutParams.MATCH_PARENT,
           ViewGroup.LayoutParams.MATCH_PARENT
       )
       scrollView.background = context.resources.getDrawable(R.drawable.roundwhitebg)
       scrollView.layoutParams = layoutParams
       scrollView.setPadding(16, 0, 16, 16)


       return scrollView
   }

   open fun LinearLayoutViewHelper(context: Context):LinearLayout{
       val linearLayout = LinearLayout(context)
       val linearParams = LinearLayout.LayoutParams(
           ViewGroup.LayoutParams.MATCH_PARENT,
           ViewGroup.LayoutParams.WRAP_CONTENT
       )
       linearLayout.orientation = LinearLayout.VERTICAL
       linearLayout.layoutParams = linearParams
       linearLayout.setPadding(0, 16, 0, 0)
       linearLayout.background = context.resources.getDrawable(R.drawable.roundwhitebg)

       return linearLayout
   }
    open fun DividerLineViewHelper(context: Context):LinearLayout{
        val linearLayout = LinearLayout(context)
        val linearParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.layoutParams = linearParams
        linearLayout.setPadding(0, 16, 0, 0)
        linearLayout.background = context.resources.getDrawable(R.drawable.roundwhitebg)


        return linearLayout
   }
 open fun RelativeLayoutViewHelper(context: Context):RelativeLayout{
       val linearLayout = RelativeLayout(context)
       val linearParams = RelativeLayout.LayoutParams(
           ViewGroup.LayoutParams.MATCH_PARENT,
           ViewGroup.LayoutParams.WRAP_CONTENT
       )
      // linearLayout.orientation = RelativeLayout.VERTICAL
       //linearLayout.gravity = RelativeLayout.VERTICAL
       linearLayout.layoutParams = linearParams
       linearLayout.setPadding(0, 16, 0, 0)

       return linearLayout
   }


    open fun mTableViewLayout(context: Context):TableRow{
        val taRow = TableRow(context)
        // taRow.setLayoutParams(tr.getLayoutParams());
        // taRow.setLayoutParams(tr.getLayoutParams());
        val tableRowParams = TableLayout.LayoutParams(
            TableLayout.LayoutParams.FILL_PARENT,
            TableLayout.LayoutParams.WRAP_CONTENT
        )

        val leftMargin = 0
        val topMargin = 30
        val rightMargin = 0
        val bottomMargin = 0

        tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin)
        taRow.layoutParams = tableRowParams
        taRow.setPadding(0, 10, 10, 10)
        taRow.gravity = Gravity.CENTER

        return taRow
    }

}


