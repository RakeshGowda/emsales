package com.rakesh.emsells.Helper

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitBuilderClass {

    fun getRetrofitClient():Retrofit {
        val intercepter=HttpLoggingInterceptor()
        intercepter.level=HttpLoggingInterceptor.Level.BODY
        val okHttpClient=OkHttpClient().newBuilder()
            .addInterceptor(intercepter)
            .connectTimeout(30,TimeUnit.SECONDS)
            .readTimeout(30,TimeUnit.SECONDS)
            .writeTimeout(30,TimeUnit.SECONDS)
            .addInterceptor {
                chain->
                val request = chain.request()
                val requestBuilder: Request.Builder = request.newBuilder()
                    .method(request.method(), request.body())
                val newRequest: Request = requestBuilder.build()
                chain.proceed(newRequest)
            }
            .build()



        val mRetrofitCilentBuilder =
            Retrofit.Builder().baseUrl("https://www.empulseudp.com/EmsalesAPI/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        return mRetrofitCilentBuilder
    }


}