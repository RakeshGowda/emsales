package com.rakesh.emsells.Helper

import okhttp3.Call
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface RetrofitInterface {

    //for the Side Menu List Load
    @POST("RoleManagement/GetLeftMenuCategory")
    fun sideMenuList(
        @Header("Authorization") Auth: String,
        @Body Input: HashMap<String, String>
    ): retrofit2.Call<ResponseBody>

    //for the Home List Data Fetch
    @POST("FormBuilder/DynamicFormBuilderOperationsSelect")
    fun getHomeListData(
        @Header("Authorization") Auth: String,
        @Body Input: HashMap<String, String>
    ): retrofit2.Call<ResponseBody>

    //for the get Add Form View Details
    @POST("FormBuilder/SelectData")
    fun getAddViewDetails(
        @Header("Authorization") Auth: String,
        @Body Input: HashMap<String, String>
    ): retrofit2.Call<ResponseBody>

    //for the get Add Form Drop Down Details
    @POST("FormBuilder/GetReferenceSelect")
    fun getAddFormDropDownDetails(
        @Header("Authorization") Auth: String,
        @Body Input: HashMap<String, String>
    ): retrofit2.Call<ResponseBody>


}